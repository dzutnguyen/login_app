import 'package:flutter/material.dart';

class CustomLoginForm extends StatefulWidget {
  @override
  _CustomLoginFormState createState() => _CustomLoginFormState();
}

// Data class
class _LoginData {
  String name = "";
  String password = "";
}

class _CustomLoginFormState extends State<CustomLoginForm> {
  final _formKey = GlobalKey<FormState>();
  _LoginData _data = new _LoginData();
  @override
  Widget build(BuildContext context) {
    var validatorFunc = (value) {
      if (value.isEmpty) {
        return "please enter name";
      } else {
        _data.name = value;
      }
    };
    var validatorPassword = (value) {
      if (value.isEmpty) {
        return "please enter password";
      } else {
        _data.password = value;
      }
    };
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: Image.asset(
              "images/face.png",
              width: 90,
              height: 90,
              color: Colors.green,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                labelText: "Name",
                border: OutlineInputBorder(
                  gapPadding: 3.3,
                  borderRadius: BorderRadius.circular(2.3),
                ),
              ),
              validator: validatorFunc,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  labelText: "Password",
                  border: OutlineInputBorder(
                      gapPadding: 3.3,
                      borderRadius: BorderRadius.circular(2.3))),
              validator: validatorPassword,
            ),
          ),

          //add button
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        setState(() { // call setState to change the state of the object
                          _data.name = _data.name;
                        });
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("All is good"),
                        ));
                      }
                    },
                    child: Text("Submit"),
                  ),
                ),
                RaisedButton(
                  onPressed: () {
                    _formKey.currentState.reset();
                    setState(() {
                      _data.name = "";
                    });
                  },
                  child: Text("Clear"),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Center(
              child: _data.name.isEmpty
                  ? Text("")
                  : Text(
                      "Welcome ${_data.name}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 19.0,
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
